package com.embraceinteractive.application.sharelist;


import com.embraceinteractive.core.application.BaseApplication;
import com.embraceinteractive.core.events.Event;
import com.embraceinteractive.core.interfaces.IEvent;
import com.embraceinteractive.core.interfaces.IModel;
import com.embraceinteractive.core.listeners.EventListener;
import com.embraceinteractive.core.manager.CommandEventManager;
import com.embraceinteractive.core.manager.ModelManager;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 7:01 PM
 */
public class ShareListApplication extends BaseApplication{

    private static final String TAG = ShareListApplication.class.getSimpleName();

    private ModelManager mm;
    private CommandEventManager cem;

    public final  static String EVENT_STARTUP = TAG+".EVENT_STARTUP";
    public static final String EVENT_NO_USER = TAG+".EVENT_NO_USER";
    public static final String EVENT_HAS_USER = TAG+".EVENT_HAS_USER";

    public final static String EVENT_BOOTUP =  TAG+".EVENT_BOOTUP";

    public ShareListApplication(){

        super();
        mm = new ModelManager();
        cem = new CommandEventManager();
        cem.setEventDispatcher(getEventDispatcher());
    }

    public void startup(){

       cem.getEventDispatcher().addEventListener(CommandEventManager.EVENT_COMPLETE,bootup);
       getEventDispatcher().dispatchEvent(new Event(EVENT_STARTUP));

    }

    private EventListener bootup = new EventListener(){

        @Override
        public void onEvent(IEvent event) {

            cem.getEventDispatcher().removeEventListener(CommandEventManager.EVENT_COMPLETE,bootup);
            getEventDispatcher().dispatchEvent(new Event(EVENT_BOOTUP));
        }
    };

    public IModel getModel(String __name){

        return mm.getModel(__name);
    }

   public CommandEventManager getCommandManager(){

       return cem;
   }

    public void registerModel(String __name,IModel __model){

        __model.setDispatcher(getEventDispatcher());
        __model.setContext(getApplicationContext());
        mm.addModel(__name,__model);
    }
}
