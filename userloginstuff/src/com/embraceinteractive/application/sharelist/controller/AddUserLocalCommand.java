package com.embraceinteractive.application.sharelist.controller;

import android.content.Context;
import android.content.SharedPreferences;
import com.embraceinteractive.application.sharelist.ShareListApplication;
import com.embraceinteractive.application.sharelist.model.UserModel;
import com.embraceinteractive.core.command.EventCommand;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:02 PM
 */
public class AddUserLocalCommand extends EventCommand {


    public AddUserLocalCommand(Object __target, Object __provider, Boolean __kill) {

        super(__target,__provider, __kill);
    }

    @Override
    public void execute(){

        ShareListApplication target = (ShareListApplication) this.getTarget();
        SharedPreferences preferences = target.getSharedPreferences("UserFile", Context.MODE_PRIVATE);
        UserModel userModel = (UserModel) target.getModel("user");
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("user_name", userModel.getUser().getName());
        editor.putString("user_id", userModel.getUser().get_id());
        editor.putString("user_email", userModel.getUser().getEmail());
        editor.commit();

        String name = preferences.getString("user_name",null);
    }
}
