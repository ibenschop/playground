package com.embraceinteractive.application.sharelist.controller;

import android.app.Activity;
import android.content.Intent;
import com.embraceinteractive.application.sharelist.ShareListApplication;
import com.embraceinteractive.application.sharelist.view.activity.CreateUserActivity;
import com.embraceinteractive.application.sharelist.view.activity.MainActivity;
import com.embraceinteractive.core.command.EventCommand;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:02 PM
 */
public class StartActivityCommand extends EventCommand {


    private Class activityClass;

    public StartActivityCommand(Object __target, Object __provider, Boolean __kill) {
        super(__target,__provider, __kill);


    }

    public StartActivityCommand setActivityClass(Class __activityClass ){

        activityClass = __activityClass;
        return this;
    }



    @Override
    public void execute(){

        ShareListApplication target = (ShareListApplication) this.getTarget();

        Intent myIntent = new Intent(target, activityClass);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        target.startActivity(myIntent);
    }

    @Override
    public void destroy(){

        super.destroy();
        activityClass = null;
    }
}
