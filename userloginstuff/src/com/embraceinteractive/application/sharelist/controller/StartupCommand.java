package com.embraceinteractive.application.sharelist.controller;

import com.embraceinteractive.application.sharelist.ShareListApplication;
import com.embraceinteractive.application.sharelist.model.UserModel;
import com.embraceinteractive.application.sharelist.view.activity.CreateUserActivity;
import com.embraceinteractive.application.sharelist.view.activity.ListsActivity;
import com.embraceinteractive.core.command.EventCommand;


/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:06 PM
 */

public class StartupCommand extends EventCommand {


    public StartupCommand(Object __target,Object __provider, Boolean __kill) {

        super(__target,__provider,__kill);
    }

    @Override
    public void execute() {

        //Get the reference of the application (Global)
        ShareListApplication app = (ShareListApplication) getTarget();

        //Set our Commands this is the  routing of out application
        app.getCommandManager().addCommand(ShareListApplication.EVENT_BOOTUP,
                new ValidateLocalUserCommand(app,null,true),
                "CheckingLocalUser");
        app.getCommandManager().addCommand(ShareListApplication.EVENT_NO_USER,
                new StartActivityCommand(app,null,true).
                        setActivityClass(CreateUserActivity.class),
                "StartActivityCommand");
        app.getCommandManager().addCommand(ShareListApplication.EVENT_HAS_USER,
                new StartActivityCommand(app,null,true).
                        setActivityClass(ListsActivity.class),
                "StartActivityCommand");
        app.getCommandManager().addCommand(UserModel.EVENT_USER_CREATED,
                new AddUserLocalCommand(app, null, true),
                "AddUserLocalCommand");

        //call complete
        complete();
    }

    @Override
    public void destroy() {

        super.destroy();
    }
}
