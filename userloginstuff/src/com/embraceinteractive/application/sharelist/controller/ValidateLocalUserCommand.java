package com.embraceinteractive.application.sharelist.controller;

import android.content.Context;
import android.content.SharedPreferences;
import com.embraceinteractive.application.sharelist.ShareListApplication;
import com.embraceinteractive.core.command.EventCommand;
import com.embraceinteractive.core.events.Event;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:02 PM
 */
public class ValidateLocalUserCommand extends EventCommand {


    public ValidateLocalUserCommand(Object __target, Object __provider, Boolean __kill) {
        super(__target, __provider, __kill);
    }

    @Override
    public void execute() {

        ShareListApplication target = (ShareListApplication) this.getTarget();
        SharedPreferences preferences = target.getSharedPreferences("UserFile", Context.MODE_PRIVATE);
        String name = preferences.getString("user_name", "");

        if (name == "") {

            target.getEventDispatcher().dispatchEvent(new Event(ShareListApplication.EVENT_NO_USER));
        } else {

            target.getEventDispatcher().dispatchEvent(new Event(ShareListApplication.EVENT_HAS_USER));
        }
    }
}
