package com.embraceinteractive.application.sharelist.model;

import android.content.Context;
import android.content.SharedPreferences;
import com.embraceinteractive.application.sharelist.model.valueobject.VOItem;
import com.embraceinteractive.application.sharelist.model.valueobject.VOList;
import com.embraceinteractive.core.events.Event;
import com.embraceinteractive.core.interfaces.IModel;
import com.embraceinteractive.core.model.core.JSONModel;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * User: bootstrap
 * Date: 12/10/13
 * Time: 8:48 PM
 */
public class ListModel extends JSONModel implements IModel {

    private static final String TAG = UserModel.class.getSimpleName();

    public static final String EVENT_CREATE_LIST = TAG + ".EVENT_CREATE_LIST";
    public static final String EVENT_ITEM_REMOVED = TAG + ".EVENT_ITEM_REMOVED";
    public static final String EVENT_ITEM_ADDED = TAG + ".EVENT_ITEM_ADDED";
    public static final String EVENT_LIST_REMOVED = TAG+".EVENT_LIST_REMOVED";
    public static final String EVENT_CONTRIBUTOR_ADDED = TAG+".EVENT_CONTRIBUTOR_ADDED";
    public static final String EVENT_CONTRIBUTOR_REMOVED= TAG+".EVENT_CONTRIBUTOR_REMOVED";
    public static final String EVENT_LISTS= TAG+".EVENT_LISTS";

    private static final String CREATE_LIST = "createlist";
    private static final String REMOVE_ITEM = "removeitem";
    private static final String ADD_ITEM = "additem";
    private static final String REMOVE_LIST = "removelist";
    private static final String ADD_CONTRIBUTOR = "addcontributor";
    private static final String REMOVE_CONTRIBUTOR = "removecontributor";
    private static final String GET_LISTS = "getlists";

    private HashMap<String,VOList> lists;

    private Context context;
    private SharedPreferences sharedPreferences;


    public ListModel(String __host) {

        super(__host);
        lists = new HashMap<String, VOList>();

    }

    public void getLists(String __userID){

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userID", __userID);

            JSONArray jsonArrayResult = post(GET_LISTS, jsonObject);

            //TODO this is a very expensive data translation
            for (int i = 0; i < jsonArrayResult.length(); i++) {

                JSONObject jsonObjectItem = jsonArrayResult.getJSONObject(i);
                VOList item = new Gson().fromJson(jsonObjectItem.toString(), VOList.class);

                lists.put(item.get_id(),item);
            }

            getDispatcher().dispatchEvent(new Event(EVENT_LISTS, jsonArrayResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addContributor(String __documentID, String __userID) {

        JSONObject jsonObject = new JSONObject();

        try {

            lists.get(__documentID).addContributor(__userID);
            jsonObject.put("documentID", __documentID);
            jsonObject.put("userID", __userID);
            JSONArray jsonArrayResult = post(ADD_CONTRIBUTOR, jsonObject);
            getDispatcher().dispatchEvent(new Event(EVENT_CONTRIBUTOR_ADDED, jsonArrayResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removeContributor(String __documentID,String __userID) {

        JSONObject jsonObject = new JSONObject();
        try {
            lists.get(__documentID).removeContributor(__userID);
            jsonObject.put("documentID", __documentID);
            jsonObject.put("userID",__userID);
            JSONArray jsonArrayResult = post(REMOVE_CONTRIBUTOR, jsonObject);
            getDispatcher().dispatchEvent(new Event(EVENT_CONTRIBUTOR_REMOVED, jsonArrayResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removeItem(String __documentID, VOItem __item) {

        JSONObject jsonObject = new JSONObject();
        try {
            lists.get(__documentID).removeItem(__item);
            jsonObject.put("documentID", __documentID);
            jsonObject.put("item", __item.getName());
            JSONArray jsonArrayResult = post(REMOVE_ITEM, jsonObject);
            getDispatcher().dispatchEvent(new Event(EVENT_ITEM_REMOVED, jsonArrayResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addItem(String __documentID, VOItem __item) {

        JSONObject jsonObject = new JSONObject();
        try {
            lists.get(__documentID).addItem(__item);
            jsonObject.put("documentID", __documentID);
            jsonObject.put("item", __item);
            JSONArray jsonArrayResult = post(ADD_ITEM, jsonObject);
            getDispatcher().dispatchEvent(new Event(EVENT_ITEM_ADDED, jsonArrayResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removeList(String __documentID) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("documentID", __documentID);
            lists.remove(__documentID);
            JSONArray jsonArrayResult = post(REMOVE_LIST, jsonObject);
            getDispatcher().dispatchEvent(new Event(EVENT_LIST_REMOVED, jsonArrayResult));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void createList(VOList __list) {


        try {
            Gson gson = new Gson();
            VOList list = __list;
            String jsonString = gson.toJson(__list);
            JSONObject jsonObjectList = null;
            jsonObjectList = new JSONObject(jsonString);
            JSONArray jsonArrayResult = post(CREATE_LIST, jsonObjectList);
            JSONObject jsonObjectResult = jsonArrayResult.getJSONObject(0);
            String id =  jsonObjectResult.getString("_id");
            list.set_id(id);
            lists.put(id,list);

            getDispatcher().dispatchEvent(new Event(EVENT_CREATE_LIST, jsonArrayResult));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    @Override
    public void setSharedPreferences(SharedPreferences __sharedprefrences) {
        sharedPreferences = __sharedprefrences;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }
}
