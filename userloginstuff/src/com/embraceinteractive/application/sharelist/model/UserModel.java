package com.embraceinteractive.application.sharelist.model;

import android.content.Context;
import android.content.SharedPreferences;
import com.embraceinteractive.application.sharelist.model.valueobject.VOUser;
import com.embraceinteractive.core.events.Event;
import com.embraceinteractive.core.interfaces.IModel;
import com.embraceinteractive.core.model.core.JSONModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: bootstrap
 * Date: 12/8/13
 * Time: 6:50 AM
 */
public class UserModel extends JSONModel implements IModel{

    private static final String TAG = UserModel.class.getSimpleName();

    public static final int NO_USER = 0;
    public static final int YES_USER = 1;
    public static final int FOUND_USER = 2;


    public static final String EVENT_USER_CREATED = TAG + ".EVENT_USER_CREATED";
    public static final String EVENT_USER_FOUND = TAG + ".EVENT_USER_FOUND";
    public static final String EVENT_FRIENDS = TAG + ".EVENT_FRIENDS";
    public static final String EVENT_FRIEND_REMOVED = TAG + ".EVENT_FRIEND_REMOVED";
    public static final String EVENT_FRIEND_ADDED = TAG +".EVENT_FRIEND_ADDED";


    private static final String CREATE_USER = "createme";
    private static final String FIND_USER = "findme";
    private static final String GET_FRIENDS = "friends";
    private static final String REMOVE_FRIEND = "removefriend";
    private static final String ADD_FRIEND ="addfriend";



    private VOUser user = new VOUser();
    private CopyOnWriteArrayList<VOUser> friends;
    private SharedPreferences sharedPreferences;

    private Context context;


    public UserModel(String __host) {

        super(__host);
        friends = new CopyOnWriteArrayList<VOUser>();
    }



    public void createUser(String __name, String __email) {


        try {

            Gson gson = new Gson();
            user.setEmail(__email);
            user.setName(__name);
            String jsonString = gson.toJson(user);
            JSONObject jsonObject = new JSONObject(jsonString);

            JSONArray jsonArrayResult = post(CREATE_USER, jsonObject);


            JSONObject jsonObjectResult = jsonArrayResult.getJSONObject(0);
            user.set_id(jsonObjectResult.getString("_id"));

            getDispatcher().dispatchEvent(new Event(EVENT_USER_CREATED, user));
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public void findUser(String __name) {


        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put("name", __name);
            JSONArray jsonArrayResult = post(FIND_USER, jsonObject);
            JSONObject jsonObjectResult =  jsonArrayResult.getJSONObject(0);
            user = new Gson().fromJson(jsonObjectResult.toString(), VOUser.class);
            user.set_id(jsonObjectResult.getString("_id"));
            getDispatcher().dispatchEvent(new Event(EVENT_USER_FOUND));

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void addFriend(String __friendID) {

        try {

            user.addFriend(__friendID);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("friendID", __friendID);
            jsonObject.put("userID", user.get_id());
            JSONArray jsonArrayResult = post(ADD_FRIEND, jsonObject);
            getDispatcher().dispatchEvent(new Event(EVENT_FRIEND_ADDED, jsonArrayResult));
        } catch (JSONException e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void removeFriend(String __friendID) {

        try {

            user.removeFriend(__friendID);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("friendID", __friendID);
            jsonObject.put("userID", user.get_id());
            JSONArray jsonArrayResult = post(REMOVE_FRIEND, jsonObject);
            getDispatcher().dispatchEvent(new Event(EVENT_FRIEND_REMOVED, jsonArrayResult));
        } catch (JSONException e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void getFriendsList() {

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userID", user.get_id());
            JSONArray jsonArrayResult = post(GET_FRIENDS, jsonObject);
            friends = new Gson().fromJson(jsonArrayResult.toString(), new TypeToken<CopyOnWriteArrayList<VOUser>>(){}.getType());
            getDispatcher().dispatchEvent(new Event(EVENT_FRIENDS));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public VOUser getUser() {
        return user;
    }


    @Override
    public void setContext(Context __context) {
        context = __context;
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    @Override
    public void setSharedPreferences(SharedPreferences __sharedprefrences) {
        sharedPreferences = __sharedprefrences;
    }
}
