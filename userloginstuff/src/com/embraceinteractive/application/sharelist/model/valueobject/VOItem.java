package com.embraceinteractive.application.sharelist.model.valueobject;

/**
 * User: bootstrap
 * Date: 12/11/13
 * Time: 8:11 PM
 */
public class VOItem {

    private String name;
    private boolean checked;

    public VOItem(String __name,Boolean __checked){

        name = __name;
        checked = __checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean equals(VOItem __item) {

        if(__item.getName() == name){

            return true;
        }else{

            return false;
        }
    }
}
