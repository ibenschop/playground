package com.embraceinteractive.application.sharelist.model.valueobject;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: bootstrap
 * Date: 12/10/13
 * Time: 8:23 PM
 */
public class VOList {

    private String _id;
    private String name;
    private String ownerID;
    private CopyOnWriteArrayList<VOItem> list;
    private CopyOnWriteArrayList<String> contributors;

    public VOList() {
        list = new CopyOnWriteArrayList<VOItem>();
        contributors = new CopyOnWriteArrayList<String>();
    }

    public CopyOnWriteArrayList getList (){

        return list;
    }

    public CopyOnWriteArrayList getContributors(){

        return contributors;
    }

    public void addItem(VOItem item){

       list.add(item);
    }

    public void removeItem(VOItem item){

        list.remove(item);
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }





    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public void addContributor(String userID){

        contributors.add(userID);
    }

    public void removeContributor(String userID){

       contributors.remove(userID);
    }

    public boolean equals(VOUser __contributor) {

        if(__contributor.get_id() == _id){

            return true;
        }else{

            return false;
        }
    }
}
