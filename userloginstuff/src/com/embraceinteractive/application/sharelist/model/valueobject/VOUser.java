package com.embraceinteractive.application.sharelist.model.valueobject;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: bootstrap
 * Date: 12/8/13
 * Time: 7:34 AM
 */
public class VOUser {

    private String name;
    private String _id;
    private String email;
    private CopyOnWriteArrayList<String> friends;

    public VOUser(){

        friends = new CopyOnWriteArrayList<String>();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addFriend(String __friend){

        friends.add(__friend);
    }

    public void removeFriend(String __friend){

        friends.remove(__friend);
    }

    public CopyOnWriteArrayList<String> getFriends() {
        return friends;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean equals(VOUser __user) {

        if(__user.get_id() == _id){

            return true;
        }else{

            return false;
        }
    }
}
