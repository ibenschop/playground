package com.embraceinteractive.application.sharelist.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.EditText;
import com.embraceinteractive.application.sharelist.R;
import com.embraceinteractive.application.sharelist.ShareListApplication;
import com.embraceinteractive.application.sharelist.model.UserModel;




public class CreateUserActivity extends Activity {


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.createuser);

        //Get application
        final ShareListApplication app = (ShareListApplication) getApplication();
        final UserModel userModel = (UserModel) app.getModel("user");

        Button buttonCreate = (Button) findViewById(R.id.buttonCreate) ;
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText editTextName = (EditText) findViewById(R.id.editName);
                EditText editTextEmail = (EditText) findViewById(R.id.editEmail);

                userModel.createUser(editTextName.getText().toString(),editTextEmail.getText().toString());
            }
        });
    }
}
