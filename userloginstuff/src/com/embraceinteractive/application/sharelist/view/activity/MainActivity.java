package com.embraceinteractive.application.sharelist.view.activity;

import android.app.Activity;
import android.os.Bundle;
import com.embraceinteractive.application.sharelist.R;
import com.embraceinteractive.application.sharelist.ShareListApplication;
import com.embraceinteractive.application.sharelist.controller.AddUserLocalCommand;
import com.embraceinteractive.application.sharelist.controller.StartActivityCommand;
import com.embraceinteractive.application.sharelist.controller.StartupCommand;
import com.embraceinteractive.application.sharelist.controller.ValidateLocalUserCommand;
import com.embraceinteractive.application.sharelist.model.*;


public class MainActivity extends Activity {

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        //Call super
        super.onCreate(savedInstanceState);

        //Set the content view for this activity
        setContentView(R.layout.main);

        //get our application
        ShareListApplication app = (ShareListApplication) getApplication();

        //Setup  models and register them to the application
        String api = getResources().getString(R.string.app_local_api);
        app.registerModel("user",new UserModel(api));
        app.registerModel("list",new UserModel(api));

        //Call the startup command and start the rest of the application
        app.getCommandManager().addCommand(
                ShareListApplication.EVENT_STARTUP,
                new StartupCommand(app,null, true),
                "StartupApplication"
        );

        //Startup the app
        app.startup();
    }
}
