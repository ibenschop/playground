package com.embraceinteractive.core.application;

import android.app.Application;
import com.embraceinteractive.core.dispatcher.EventDispatcher;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:08 PM
 */
public class BaseApplication extends Application {

    private EventDispatcher eventDispatcher;

    public BaseApplication(){

        super();
        eventDispatcher = new EventDispatcher();
    }

    public EventDispatcher getEventDispatcher() {
        return eventDispatcher;
    }

    public void setEventDispatcher(EventDispatcher eventDispatcher) {
        this.eventDispatcher = eventDispatcher;
    }
}
