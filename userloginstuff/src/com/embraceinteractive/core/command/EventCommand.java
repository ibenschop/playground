package com.embraceinteractive.core.command;

import com.embraceinteractive.core.dispatcher.EventDispatcher;
import com.embraceinteractive.core.events.Event;
import com.embraceinteractive.core.interfaces.ICommand;
import com.embraceinteractive.core.interfaces.IEvent;
import com.embraceinteractive.core.listeners.EventListener;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:20 PM
 */
public class EventCommand extends EventDispatcher implements ICommand {

    private static final String TAG = EventCommand.class.getSimpleName();
    private boolean kill = false;
    public final static String EVENT_COMPLETE =  TAG+".EVENT_COMPLETE";
    private String event;
    private Object target;
    private String tag;
    private Object provider;

    public EventCommand(Object __target,Object __provider, Boolean __kill){

        kill = __kill;
        target = __target;
        provider = __provider;
    }

    public EventListener handler = new EventListener() {

        @Override
        public void onEvent(IEvent event) {

            execute();
        }
    };


    @Override
    public void execute() {

    }

    @Override
    public void destroy() {

       target = null;
        provider = null;
    }

    @Override
    public void complete() {

        dispatchEvent(new Event(EVENT_COMPLETE));

        if(kill == true){

            destroy();
        }

    }

    @Override
    public void undo() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isKill() {
        return kill;
    }

    public void setKill(boolean kill) {
        this.kill = kill;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getProvider() {
        return provider;
    }
}
