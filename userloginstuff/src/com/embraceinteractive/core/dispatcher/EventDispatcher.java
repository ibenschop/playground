package com.embraceinteractive.core.dispatcher;

import com.embraceinteractive.core.interfaces.IDispatcher;
import com.embraceinteractive.core.interfaces.IEvent;
import com.embraceinteractive.core.interfaces.IEventListener;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * User: bootstrap
 * Date: 12/9/13
 * Time: 8:31 PM
 */
public class EventDispatcher implements IDispatcher {


    private HashMap<String, CopyOnWriteArrayList<IEventListener>> _eventListenerMapping;
    private IDispatcher _target;
    private static final String TAG = IDispatcher.class.getSimpleName();

    public EventDispatcher() {
        this(null);
    }

    public EventDispatcher(IDispatcher __target) {
        _eventListenerMapping = new HashMap<String, CopyOnWriteArrayList<IEventListener>>();
        this._target = (_target != null) ? __target : this;
    }

    @Override
    public void addEventListener(String __type, IEventListener __eventListener) {
        synchronized (_eventListenerMapping) {
            CopyOnWriteArrayList<IEventListener> list = _eventListenerMapping.get(__type);
            if (list == null) {
                list = new CopyOnWriteArrayList<IEventListener>();
                _eventListenerMapping.put(__type, list);
            }
            list.add(__eventListener);
        }
    }

    @Override
    public void removeEventListener(String __type, IEventListener __eventListener) {

        synchronized (_eventListenerMapping) {
            CopyOnWriteArrayList<IEventListener> list = _eventListenerMapping.get(__type);
            if (list == null) return;
            list.remove(__eventListener);
            if (list.size() == 0) {
                _eventListenerMapping.remove(__type);
            }
        }
    }

    @Override
    public boolean hasEventListener(String type, IEventListener listener) {
        synchronized (_eventListenerMapping) {
            CopyOnWriteArrayList<IEventListener> list = _eventListenerMapping.get(type);
            if (list == null) return false;
            return list.contains(listener);
        }
    }

    @Override
    public void dispatchEvent(IEvent event) {
        if (event == null) {
            //Log.e(TAG, "can not dispatch null event");
            return;
        }
        String type = event.getType();
        event.setTarget(_target);

        CopyOnWriteArrayList<IEventListener> list;
        synchronized (_eventListenerMapping) {
            list = _eventListenerMapping.get(type);
        }
        if (list == null) return;
        for (IEventListener l : list) {
            l.onEvent(event);
        }
    }
}
