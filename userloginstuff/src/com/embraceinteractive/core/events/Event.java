package com.embraceinteractive.core.events;

import com.embraceinteractive.core.interfaces.IEvent;

/**
 * User: bootstrap
 * Date: 12/9/13
 * Time: 8:45 PM
 */
public class Event implements IEvent {

    protected String _type;
    protected Object _target;
    protected Object _payload;

    public Event(String __type){

        _type = __type;
    }

    public Event(String __type,Object __payload){

        _type = __type;
        _payload = __payload;
    }






    @Override
    public String getType() {
        return _type;
    }

    @Override
    public Object getPayload() {
        return _payload;
    }


    @Override
    public void setTarget(Object __target) {

         _target = __target ;
    }

    @Override
    public Object getTarget() {
        return _target;
    }
}
