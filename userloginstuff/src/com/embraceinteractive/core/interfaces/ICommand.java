package com.embraceinteractive.core.interfaces;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:01 PM
 */
public interface ICommand {

    public void execute();
    public void destroy();
    public void complete();
    public void undo();
}
