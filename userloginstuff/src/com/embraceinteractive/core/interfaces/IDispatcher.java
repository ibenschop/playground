package com.embraceinteractive.core.interfaces;

/**
 * User: bootstrap
 * Date: 12/9/13
 * Time: 8:22 PM
 */
public interface IDispatcher {

    void addEventListener(String __type,IEventListener __eventListener);
    void removeEventListener(String __type,IEventListener listener);
    boolean hasEventListener(String type, IEventListener listener);
    void dispatchEvent(IEvent event);
}
