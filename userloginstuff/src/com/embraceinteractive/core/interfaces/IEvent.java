package com.embraceinteractive.core.interfaces;

/**
 * User: bootstrap
 * Date: 12/9/13
 * Time: 8:27 PM
 */
public interface IEvent {


    public String getType();
    public Object getPayload();
    public void setTarget(Object __target);
    public Object getTarget() ;
}
