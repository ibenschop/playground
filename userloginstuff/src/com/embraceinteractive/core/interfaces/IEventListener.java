package com.embraceinteractive.core.interfaces;

/**
 * User: bootstrap
 * Date: 12/9/13
 * Time: 8:26 PM
 */
public interface IEventListener {

    public void onEvent(IEvent event);
}
