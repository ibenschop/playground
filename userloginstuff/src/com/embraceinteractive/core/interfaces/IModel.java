package com.embraceinteractive.core.interfaces;

import android.content.Context;
import android.content.SharedPreferences;
import com.embraceinteractive.core.dispatcher.EventDispatcher;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 4:25 AM
 */
public interface IModel {

    public void setContext(Context __context);
    public Context getContext();
    public SharedPreferences getSharedPreferences();
    public void setSharedPreferences(SharedPreferences __sharedprefrences);
    public IDispatcher getDispatcher();
    public void setDispatcher(IDispatcher __dispatcher);
}
