package com.embraceinteractive.core.manager;

import com.embraceinteractive.core.command.EventCommand;
import com.embraceinteractive.core.dispatcher.EventDispatcher;
import com.embraceinteractive.core.events.Event;
import com.embraceinteractive.core.interfaces.ICommand;
import com.embraceinteractive.core.interfaces.IEvent;
import com.embraceinteractive.core.listeners.EventListener;

import java.util.HashMap;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 8:01 PM
 */
public class CommandEventManager {

    private static final String TAG = CommandEventManager.class.getSimpleName();

    public static final String EVENT_COMPLETE = TAG+".EVENT_COMPLETE";

    private EventDispatcher eventDispatcher;
    private HashMap<String,ICommand> mapping;


    public CommandEventManager(){

        mapping = new HashMap<String, ICommand>();
    }

    public EventListener eventCompleteHandler = new EventListener() {

        @Override
        public void onEvent(IEvent event) {

            EventCommand target = (EventCommand) event.getTarget();
            String name = target.getClass().getSimpleName();

            if(target.isKill()){

                removeCommand(target.getEvent(),target.getTag());
            }

            eventDispatcher.dispatchEvent(new Event(EVENT_COMPLETE));
        }
    };


    public void addCommand(String __event, EventCommand __command,String __tag){

        __command.setEvent(__event);
        __command.setTag(__tag);
        __command.addEventListener(EventCommand.EVENT_COMPLETE,eventCompleteHandler);
        mapping.put(__tag,__command);
        eventDispatcher.addEventListener(__event,__command.handler);

    }

    public void removeCommand(String __event,String __tag){

        EventCommand command = (EventCommand) mapping.get(__tag);
        eventDispatcher.removeEventListener(__event,command.handler);
        mapping.remove(__tag);
    }



    public EventDispatcher getEventDispatcher() {

        return eventDispatcher;
    }

    public void setEventDispatcher(EventDispatcher eventDispatcher) {
        this.eventDispatcher = eventDispatcher;
    }
}
