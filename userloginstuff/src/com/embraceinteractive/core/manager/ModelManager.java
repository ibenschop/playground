package com.embraceinteractive.core.manager;

import com.embraceinteractive.core.interfaces.IModel;

import java.util.HashMap;

/**
 * User: bootstrap
 * Date: 12/12/13
 * Time: 4:22 AM
 */
public class ModelManager {

    private HashMap<String,IModel> models;


    public ModelManager(){

         models = new HashMap<String, IModel>();
    }

    public void addModel(String __name, IModel __model){

         models.put(__name,__model);
    }

    public  IModel getModel(String __name){

         return models.get(__name);
    }

    public void removeModel(String __name){

        models.remove(__name);
    }


}
