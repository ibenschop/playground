package com.embraceinteractive.core.model.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import com.embraceinteractive.core.dispatcher.EventDispatcher;
import com.embraceinteractive.core.interfaces.IDispatcher;
import com.embraceinteractive.core.interfaces.IModel;
import com.embraceinteractive.core.utils.ConvertStreamToString;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URI;
import java.util.zip.GZIPInputStream;

/**
 * User: bootstrap
 * Date: 12/9/13
 * Time: 2:02 AM
 */
public class JSONModel implements IModel {

    private final DefaultHttpClient _httpClient = new DefaultHttpClient();
    private HttpPost _httpPost = new HttpPost();
    private final HttpGet _httpGet = new HttpGet();
    private String _host;
    private IDispatcher dispatcher;

    public JSONModel(String host) {

        _host = host;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setHeaders();
    }

    private void setHeaders() {

        _httpPost.setHeader("Accept", "application/json");
        _httpPost.setHeader("Content-type", "application/json");
        _httpPost.setHeader("Accept-Encoding", "gzip");

        _httpGet.setHeader("Accept", "application/json");
        _httpGet.setHeader("Content-type", "application/json");
        _httpGet.setHeader("Accept-Encoding", "gzip");

    }

    protected JSONArray post(String __path, JSONObject __payload) {

        StringEntity stringEntity;

        try {

            stringEntity = new StringEntity(__payload.toString());
            _httpPost.setURI(new URI(_host + "/" + __path));
            _httpPost.setEntity(stringEntity);
            HttpResponse httpResponse = (HttpResponse) _httpClient.execute(_httpPost);

            HttpEntity httpEntity = httpResponse.getEntity();

            int statusCode = httpResponse.getStatusLine().getStatusCode();

            if (statusCode == 200) {

                if (httpEntity != null) {

                    InputStream inputStream = httpEntity.getContent();
                    Header contentEncoding = httpResponse.getFirstHeader("Content-Encoding");

                    if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                        inputStream = new GZIPInputStream(inputStream);
                    }

                    String resultString = ConvertStreamToString.Convert(inputStream, false);
                    inputStream.close();

                    JSONArray jsonObjectReceived = new JSONArray(resultString);


                    return jsonObjectReceived;
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

    public String get_host() {
        return _host;
    }

    public void set_host(String _host) {
        this._host = _host;
    }

    @Override
    public void setContext(Context __context) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Context getContext() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setSharedPreferences(SharedPreferences __sharedprefrences) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public IDispatcher getDispatcher() {
        return dispatcher;
    }

    @Override
    public void setDispatcher(IDispatcher __dispatcher) {
        dispatcher = __dispatcher;
    }


}
