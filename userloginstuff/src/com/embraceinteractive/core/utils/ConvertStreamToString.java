package com.embraceinteractive.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * User: bootstrap
 * Date: 12/9/13
 * Time: 2:04 AM
 */
public class ConvertStreamToString {

    public static String Convert(InputStream __stream,Boolean __removeWrapping){

        BufferedReader reader = new BufferedReader(new InputStreamReader(__stream));
        StringBuilder stringBuilder = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                __stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String result = stringBuilder.toString();

        if(__removeWrapping == true){

            result = result.substring(1,result.length()-1);
        }
        return result;
    }
}
